Source: python-dbfread
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-sphinx <!nodoc>,
               sphinx-common
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-dbfread
Vcs-Git: https://salsa.debian.org/python-team/packages/python-dbfread.git
Homepage: https://dbfread.readthedocs.org/

Package: python3-dbfread
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-dbfread-doc <!nodoc>
Description: read DBF Files with Python
 DBF is a file format used by databases such dBase, Visual FoxPro, and
 FoxBase+. This library reads DBF files and returns the data as native
 Python data types for further processing. It is primarily intended for
 batch jobs and one-off scripts.
 .
 This package provides the modules for Python 3.

Package: python-dbfread-doc
Architecture: all
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Built-Using: ${sphinxdoc:Built-Using}
Description: documentation for dbfread
 DBF is a file format used by databases such dBase, Visual FoxPro, and
 FoxBase+. This library reads DBF files and returns the data as native
 Python data types for further processing. It is primarily intended for
 batch jobs and one-off scripts.
 .
 This package provides the documentation.
Build-Profiles: <!nodoc>
Multi-Arch: foreign
